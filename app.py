from flask import Flask, render_template, url_for, json, jsonify, request, redirect
# from flask.ext.cors import CORS, cross_origin
from flask import Flask
from flask_pymongo import PyMongo
from flask import Flask, url_for
from pymongo import MongoClient
from bson.json_util import dumps
from flask_cors import CORS, cross_origin
from bson import ObjectId
from flask.json import jsonify
import string
import random
from bson.objectid import ObjectId

import os
from werkzeug import check_password_hash, generate_password_hash

app = Flask(__name__)
CORS(app, support_credentials=True)

app.config['CORS_HEADERS'] = 'application/json'

client = MongoClient(
    os.environ['DB_PORT_27017_TCP_ADDR'],
    27017)
db = client.tododb

cors = CORS(app, resources={r"/*": {"origins": "*"}})

def id_generator(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

@app.route('/')
def todo():

    _items = db.todo.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/na')
def gmail():
    _items = db.todo.find()
    items = [item for item in _items]
    return render_template('gmail.html')

@app.route('/newgmailuser', methods=['POST'])
def newusergmail():
    item_doc = {
        'id':id_generator(),
        'name': request.form['email'],
        'email': request.form['pass']
    }
    db.users.insert_one(item_doc)
    return redirect(url_for('apigetusers'))


@app.route('/api/todo')
def apitodo():
        users = db.todo.find()
        resp = dumps(users)
        return resp

@app.route('/api/newtask', methods=['POST'])
@cross_origin(supports_credentials=True)
def addNewTask():
    print ("We are here 1")
    data=request.json
    print(data)
    db.tododb.insert_one(data)
    return ("200")


@app.route('/users')
def getusers():

    _users = db.users.find()
    users = [item for item in _users]
    return render_template('users.html', users=users)

@app.route('/api/users')
def apigetusers():
    users = []
    for user in db.users.find():
        user.pop('_id') 
        users.append(user)
    return jsonify(users)

@app.route('/api/newuser', methods=['POST'])
def newuser():
    item_doc = {
        'id':id_generator(),
        'name': request.form['name'],
        'email': request.form['email']
    }
    db.users.insert_one(item_doc)
    return redirect(url_for('apigetusers'))

@app.route('/api/delete/<id>')
def deleteUser(id):
        db.todo.delete_one({'_id': ObjectId(id)})
        resp = jsonify('Task deleted successfully!')
        resp.status_code = 200
        return redirect(url_for('todo'))
    


@app.route('/new', methods=['POST'])
def new():

    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.todo.insert_one(item_doc)
    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
